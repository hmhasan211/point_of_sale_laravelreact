<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name'=>"Admin",
            'phone'=>'12345678',
            'email'=>'admin@gmail.com',
            'password'=>Hash::make('admin123'),
            'role_id'=> 1
        ];

        User::create($data);
    }
}
