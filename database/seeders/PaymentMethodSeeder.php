<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_methods = [
            [
                'name' => 'Cash',
                'status' => 1,
                'account_number'=>''
            ],
            [
                'name' => 'bKash',
                'status' => 1,
                'account_number'=>'01627047063'
            ],
            [
                'name' => 'Nagad',
                'status' => 1,
                'account_number'=>'01627047063'
            ],
            [
                'name' => 'RockeT',
                'status' => 0,
                'account_number'=>'01627047063'
            ],
        ];
        PaymentMethod::query()->insert($payment_methods);
    }
}
