<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShopEditResource;
use App\Http\Resources\ShopResource;
use App\Models\Address;
use App\Models\Shop;
use App\Http\Requests\StoreShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $input)
    {
        $allData = (new Shop())->getAllData($input->all());
        return ShopResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShopRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreShopRequest $request)
    {
//        return $request->all();
        $shop = (new Shop())->prepareData($request->all(),auth());
        $address = (new Address())->prepareData($request->all());

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time();
            $shop['avatar'] = ImageUploadManager::processImageUpload(
                $request->input('avatar'),
                $avatarName,
                Shop::MAIN_WIDTH,
                Shop::MAIN_HEIGHT,
                Shop::MAIN_IMAGE_PATH,
                Shop::THUMB_IMAGE_PATH,
                Shop::THUMB_WIDTH,
                Shop::THUMB_HEIGHT
            );
        }
        try {
            DB::beginTransaction();
            $shop = Shop::create($shop);
            $shop->address()->create($address);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            if (isset($shop['avatar'])){
                ImageUploadManager::deletePhoto(Shop::MAIN_IMAGE_PATH,$shop['avatar']);
                ImageUploadManager::deletePhoto(Shop::THUMB_IMAGE_PATH,$shop['avatar']);
            }
            info('SHOP_STORE_FAILED',['shop'=>$shop,'address'=>$address,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop  $shop
     * @return ShopEditResource
     */
    public function show(Shop $shop)
    {
        return new ShopEditResource($shop);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShopRequest  $request
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateShopRequest $request, Shop $shop)
    {
        $EditData = (new Shop())->prepareData($request->all(),auth());
        $addressEditData = (new Address())->prepareData($request->all());

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time();
            $EditData['avatar'] = ImageUploadManager::processImageUpload(
                $request->input('avatar'),
                $avatarName,
                Shop::MAIN_WIDTH,
                Shop::MAIN_HEIGHT,
                Shop::MAIN_IMAGE_PATH,
                Shop::THUMB_IMAGE_PATH,
                Shop::THUMB_WIDTH,
                Shop::THUMB_HEIGHT
            );
        }

        try {
            DB::beginTransaction();
            $shop->update($EditData);
            $shopAddress = $shop->address()->update($addressEditData);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            info('SHOP_UPDATE_FAILED',['shop'=>$shop,'address'=>$shopAddress,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Shop $shop)
    {
        if (!empty($shop->avatar)){
            ImageUploadManager::deletePhoto(Shop::MAIN_IMAGE_PATH,$shop['avatar']);
            ImageUploadManager::deletePhoto(Shop::THUMB_IMAGE_PATH,$shop['avatar']);
        }
        (new Address())->deleteAddressByShopId($shop);
        $shop->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'success']);
    }

    public function getShopList()
    {
        $shopList = (new Shop())->getShopIdAndName();
        return response()->json($shopList);
    }
}
