<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryEditResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Manager\ImageUploadManager;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $input)
    {
        $allData = (new Category)->getAllData($input->all());
        return CategoryResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCategoryRequest $request)
    {
        $requestData = $request->except('avatar');
         $requestData['user_id'] = auth()->id();
         $requestData['slug'] = Str::slug($request->input('slug'));

         if($request->has('avatar')){
             $file = $request->input('avatar');
             $requestData['avatar'] = $this->processImageUpload($file, $requestData['slug']);
         }
         (new Category())->storeCategory($requestData);
         return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return CategoryEditResource
     */
    final public function show(Category $category)
    {
        return new CategoryEditResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCategoryRequest  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $requestUpdateData = $request->except('avatar');
        $requestUpdateData['user_id'] = auth()->id();
        $requestUpdateData['status'] = $request->input('status');
        $requestUpdateData['slug'] = Str::slug($request->input('slug'));

        if($request->has('avatar')){
            $requestUpdateData['avatar'] = $this->processImageUpload($request->input('avatar'),$requestUpdateData['slug'],$category->avatar);
        }

        $category->update($requestUpdateData);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        if(!empty($category->avatar)){
            ImageUploadManager::deletePhoto(Category::IMAGE_PATH,$category->avatar);
            ImageUploadManager::deletePhoto(Category::THUMB_IMAGE_PATH,$category->avatar);
        }
        $category->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'warning']);
    }

    public function getCategoryList()
    {
        $categories = (new Category())->getCategoryIdAndName();
        return response()->json($categories);
    }

    private function processImageUpload($file, $name ,$existing_avatar= null)
    {
        $width = 800;
        $height = 800;
        $width_thumb = 150;
        $height_thumb = 150;
        $path  = Category::IMAGE_PATH;
        $path_thumb = Category::THUMB_IMAGE_PATH;
        if(!empty($existing_avatar))
        {
            ImageUploadManager::deletePhoto(Category::IMAGE_PATH,$existing_avatar);
            ImageUploadManager::deletePhoto(Category::THUMB_IMAGE_PATH,$existing_avatar);
        }
            $avatar_name = ImageUploadManager::uploadImage($name, $width,$height,$path,$file);
            ImageUploadManager::uploadImage($name, $width_thumb,$height_thumb,$path_thumb,$file);
            return $avatar_name;
        }
}
