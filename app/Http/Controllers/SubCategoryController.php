<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubCategoryEditResource;
use App\Http\Resources\SubCategoryResource;
use App\Models\SubCategory;
use App\Http\Requests\StoreSubCategoryRequest;
use App\Http\Requests\UpdateSubCategoryRequest;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $input)
    {
        $allData = (new SubCategory())->getAllData($input->all());
        return SubCategoryResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubCategoryRequest  $request
     * @return Response
     */
    public function store(StoreSubCategoryRequest $request): Response
    {
        $requestData = $request->except('avatar');
        $requestData['user_id'] = auth()->id();
        $requestData['slug'] = Str::slug($request->input('slug'));

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $requestData['avatar'] = $this->processImageUpload($file, $requestData['slug']);
        }
        (new SubCategory())->storeSubCategory($requestData);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return Response
     */
    public function show(SubCategory $subCategory)
    {
        return new SubCategoryEditResource($subCategory);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSubCategoryRequest  $request
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateSubCategoryRequest $request, SubCategory $subCategory)
    {
        $requestUpdateData = $request->except('avatar');
        $requestUpdateData['user_id'] = auth()->id();
        $requestUpdateData['status'] = $request->input('status');
        $requestUpdateData['slug'] = Str::slug($request->input('slug'));

        if($request->has('avatar')){
            $requestUpdateData['avatar'] = $this->processImageUpload($request->input('avatar'),$requestUpdateData['slug'],$subCategory->avatar);
        }

        $subCategory->update($requestUpdateData);
        return response()->json(['msg'=>'Data has been updated!','cls'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return Response
     */
    public function destroy(SubCategory $subCategory)
    {
        if(!empty($subCategory->avatar)){
            ImageUploadManager::deletePhoto(SubCategory::IMAGE_PATH,$subCategory->avatar);
            ImageUploadManager::deletePhoto(SubCategory::THUMB_IMAGE_PATH,$subCategory->avatar);
        }
        $subCategory->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'warning']);
    }

    private function processImageUpload($file, $name ,$existing_avatar= null)
    {
        $width = 800;
        $height = 800;
        $width_thumb = 150;
        $height_thumb = 150;
        $path  = SubCategory::IMAGE_PATH;
        $path_thumb = SubCategory::THUMB_IMAGE_PATH;
        if(!empty($existing_avatar))
        {
            ImageUploadManager::deletePhoto(SubCategory::IMAGE_PATH,$existing_avatar);
            ImageUploadManager::deletePhoto(SubCategory::THUMB_IMAGE_PATH,$existing_avatar);
        }
        $avatar_name = ImageUploadManager::uploadImage($name, $width,$height,$path,$file);
        ImageUploadManager::uploadImage($name, $width_thumb,$height_thumb,$path_thumb,$file);
        return $avatar_name;
    }

    public function getSubCategoryList($cat_id)
    {
        $categories = (new SubCategory())->getSubCategoryIdAndName($cat_id);
        return response()->json($categories);
    }
}
