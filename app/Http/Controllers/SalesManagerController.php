<?php

namespace App\Http\Controllers;

use App\Http\Resources\SalesManagerEditResource;
use App\Http\Resources\SalesManagerResource;
use App\Models\Address;
use App\Models\SalesManager;
use App\Http\Requests\StoreSalesManagerRequest;
use App\Http\Requests\UpdateSalesManagerRequest;
use App\Manager\ImageUploadManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SalesManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $input)
    {
        $allData = (new SalesManager())->getAllData($input->all());
        return SalesManagerResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSalesManagerRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSalesManagerRequest $request)
    {
//        return $request->all();
        $salesManagerData = (new SalesManager())->prepareData($request->all(),auth());
        $address = (new Address())->prepareData($request->all());

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time().'-'.random_int(10000,99999);
            $salesManagerData['avatar'] = ImageUploadManager::processImageUpload(
                $request->input('avatar'),
                $avatarName,
                SalesManager::MAIN_WIDTH,
                SalesManager::MAIN_HEIGHT,
                SalesManager::MAIN_IMAGE_PATH,
                SalesManager::THUMB_IMAGE_PATH,
                SalesManager::THUMB_WIDTH,
                SalesManager::THUMB_HEIGHT
            );
        }
        if($request->has('nid_photo')){
            $file = $request->input('nid_photo');
            $avatarName = Str::slug($request->name.'-nid-'.$request->nid.'-'.time());
            $salesManagerData['nid_photo'] = ImageUploadManager::processImageUpload(
                $file,
                $avatarName,
                SalesManager::MAIN_WIDTH,
                SalesManager::MAIN_HEIGHT,
                SalesManager::MAIN_IMAGE_PATH,
                SalesManager::THUMB_IMAGE_PATH,
                SalesManager::THUMB_WIDTH,
                SalesManager::THUMB_HEIGHT
            );
        }
        try {
            DB::beginTransaction();
            $salesManagerData = SalesManager::create($salesManagerData);
            $salesManagerData->address()->create($address);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            if (isset($salesManagerData['avatar'])){
                ImageUploadManager::deletePhoto(SalesManager::MAIN_IMAGE_PATH,$salesManagerData['avatar']);
                ImageUploadManager::deletePhoto(SalesManager::THUMB_IMAGE_PATH,$salesManagerData['avatar']);
            }
            info('S_MANAGER_STORE_FAILED',['SalesManager'=>$salesManagerData,'address'=>$address,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param SalesManager $salesManager
     * @return SalesManagerEditResource
     */
    public function show(SalesManager  $salesManager)
    {
        return new SalesManagerEditResource($salesManager);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSalesManagerRequest $request
     * @param SalesManager $salesManager
     * @return JsonResponse
     */
    public function update(UpdateSalesManagerRequest $request, SalesManager  $salesManager)
    {
        $EditData = (new SalesManager())->prepareData($request->all(),auth());
        $addressEditData = (new Address())->prepareData($request->all());

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time();
            $EditData['avatar'] = ImageUploadManager::processImageUpload(
                $request->input('avatar'),
                $avatarName,
                SalesManager::MAIN_WIDTH,
                SalesManager::MAIN_HEIGHT,
                SalesManager::MAIN_IMAGE_PATH,
                SalesManager::THUMB_IMAGE_PATH,
                SalesManager::THUMB_WIDTH,
                SalesManager::THUMB_HEIGHT
            );
        }
        if($request->has('nid_photo')){
            $file = $request->input('nid_photo');
            $avatarName = Str::slug($request->name.'-nid-'.$request->nid.'-'.time());
            $EditData['nid_photo'] = ImageUploadManager::processImageUpload(
                $request->input('nid_photo'),
                $avatarName,
                SalesManager::MAIN_WIDTH,
                SalesManager::MAIN_HEIGHT,
                SalesManager::MAIN_IMAGE_PATH,
                SalesManager::THUMB_IMAGE_PATH,
                SalesManager::THUMB_WIDTH,
                SalesManager::THUMB_HEIGHT
            );
        }


        try {
            DB::beginTransaction();
            $salesManager->update($EditData);
            $Address = $salesManager->address()->update($addressEditData);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            info('S_manager_UPDATE_FAILED',['SalesManager'=>$salesManager,'address'=>$Address,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SalesManager $salesManager
     * @return JsonResponse
     */
    public function destroy(SalesManager  $salesManager)
    {
        if (!empty($salesManager->avatar)){
            ImageUploadManager::deletePhoto(SalesManager::MAIN_IMAGE_PATH,$salesManager['avatar']);
            ImageUploadManager::deletePhoto(SalesManager::THUMB_IMAGE_PATH,$salesManager['avatar']);
        }
        if (!empty($salesManager->nid_photo)){
            ImageUploadManager::deletePhoto(SalesManager::MAIN_IMAGE_PATH,$salesManager['nid_photo']);
            ImageUploadManager::deletePhoto(SalesManager::THUMB_IMAGE_PATH,$salesManager['nid_photo']);
        }
        (new Address())->deleteAddressBySalesManagerId($salesManager);
        $salesManager->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'success']);
    }
}
