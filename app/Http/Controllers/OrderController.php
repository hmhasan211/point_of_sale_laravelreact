<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderDetailsResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $orders = (new Order())->getOrders($request->all(),auth()->id());
        return OrderResource::collection($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return JsonResponse
     */
    public function store(StoreOrderRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
             $order = (new Order())->placeOrder($request->all(), auth()->user());
            DB::commit();
            return response()->json(['msg'=>'Order Placed Successfully!','cls'=>'success', 'flag'=>true, 'order_id'=>$order->id]);
        }catch (\Throwable $e){
            info('ORDER_FAILED',['msg'=>$e->getMessage(), $e]);
            DB::rollBack();
            return response()->json(['msg'=> $e->getMessage(),'cls'=>'warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return
     */
    public function show(Order $order)
    {
        $order->load(['user',
            'customer',
            'payment_method',
            'order_details',
            'shop',
            'transactions',
            'transactions.customer:id,name,phone,email',
            'transactions.payment_method:id,name,account_number,created_at',
            'transactions.transactionable:id,name,phone',

        ]);
       return new OrderDetailsResource($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
