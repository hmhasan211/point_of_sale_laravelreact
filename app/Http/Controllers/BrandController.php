<?php

namespace App\Http\Controllers;

use App\Http\Resources\BrandEditResource;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $input)
    {
        $allData = (new Brand())->getAllData($input->all());
        return BrandResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBrandRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBrandRequest $request)
    {
//        Brand::query()->truncate();
        $requestData = $request->except('avatar');
        $requestData['user_id'] = auth()->id();
        $requestData['slug'] = Str::slug($request->input('slug'));

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $requestData['avatar'] = $this->processImageUpload($file, $requestData['slug']);
        }
        (new Brand())->storeBrand($requestData);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return BrandEditResource
     */
    public function show(Brand $brand)
    {
        return new BrandEditResource($brand);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBrandRequest  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        $requestUpdateData = $request->except('avatar');
        $requestUpdateData['user_id'] = auth()->id();
        $requestUpdateData['status'] = $request->input('status');
        $requestUpdateData['slug'] = Str::slug($request->input('slug'));

        if($request->has('avatar')){
            $requestUpdateData['avatar'] = $this->processImageUpload($request->input('avatar'),$requestUpdateData['slug'],$brand->avatar);
        }

        $brand->update($requestUpdateData);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Brand $brand)
    {
        if(!empty($category->avatar)){
            ImageUploadManager::deletePhoto(Brand::IMAGE_PATH,$brand->avatar);
            ImageUploadManager::deletePhoto(Brand::THUMB_IMAGE_PATH,$brand->avatar);
        }
        $brand->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'warning']);
    }

    private function processImageUpload($file, $name ,$existing_avatar= null)
    {
        $width = 800;
        $height = 800;
        $width_thumb = 150;
        $height_thumb = 150;
        $path  = Brand::IMAGE_PATH;
        $path_thumb = Brand::THUMB_IMAGE_PATH;
        if(!empty($existing_avatar))
        {
            ImageUploadManager::deletePhoto(Brand::IMAGE_PATH,$existing_avatar);
            ImageUploadManager::deletePhoto(Brand::THUMB_IMAGE_PATH,$existing_avatar);
        }
        $avatar_name = ImageUploadManager::uploadImage($name, $width,$height,$path,$file);
        ImageUploadManager::uploadImage($name, $width_thumb,$height_thumb,$path_thumb,$file);
        return $avatar_name;
    }

    public function getBrandList()
    {
        $brandList = (new Brand())->getBrandIdAndName();
        return response()->json($brandList);
    }
}
