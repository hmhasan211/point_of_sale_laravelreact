<?php

namespace App\Http\Controllers;

use App\Models\AttributeValue;
use App\Http\Requests\StoreAttrValueRequest;
use App\Http\Requests\UpdateAttrValueRequest;

class AttrValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return (new AttributeValue())->getAttrValues();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAttrValueRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAttrValueRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->id();
        AttributeValue::query()->create($data);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AttributeValue  $attrValue
     * @return \Illuminate\Http\Response
     */
    public function show(AttributeValue $attrValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AttributeValue  $attrValue
     * @return \Illuminate\Http\Response
     */
    public function edit(AttributeValue $attrValue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAttrValueRequest  $request
     * @param  \App\Models\AttributeValue  $attrValue
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttrValueRequest $request, AttributeValue $attrValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AttributeValue  $attrValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttributeValue $attrValue)
    {
        //
    }
}
