<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductAttrRequest;
use App\Http\Requests\UpdateProductAttrRequest;
use App\Http\Resources\ProductAttrResource;
use App\Models\Attribute;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $data = (new Attribute())->getAllAtrr();
        return ProductAttrResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductAttrRequest $request)
    {
        $requestData = $request->all();
        $requestData['user_id'] = auth()->id();
        Attribute::query()->create($requestData);
        return response()->json(['msg'=>'Data has been Created!','cls'=>'success']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateProductAttrRequest $request, Attribute $productAttr)
    {

        $updateData = $request->all();
        $productAttr->update($updateData);
        return response()->json(['msg'=>'Data has been updated!','cls'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attribute  $productAttr
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Attribute $productAttr)
    {
        $productAttr->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'success']);
    }

    public function getAttributeList()
    {
        $brandList = (new Attribute())->getAttributeIdAndName();
        return response()->json($brandList);
    }
}
