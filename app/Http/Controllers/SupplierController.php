<?php

namespace App\Http\Controllers;

use App\Http\Resources\SupplierEditResource;
use App\Http\Resources\SupplierResource;
use App\Models\Address;
use App\Models\Supplier;
use App\Http\Requests\StoreSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $input)
    {
        $allData = (new Supplier())->getAllData($input->all());
        return SupplierResource::collection($allData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSupplierRequest  $request
     *
     */
    public function store(StoreSupplierRequest $request)
    {
        $supplier = (new Supplier())->prepareData($request->all(),auth());
        $address = (new Address())->prepareData($request->all());

        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time();
            $supplier['avatar'] = $this->processImageUpload($file, $avatarName);
        }
        try {
            DB::beginTransaction();
            $supplier = Supplier::create($supplier);
            $supplier->address()->create($address);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            if (isset($supplier['avatar'])){
                ImageUploadManager::deletePhoto(Supplier::IMAGE_PATH,$supplier['avatar']);
                ImageUploadManager::deletePhoto(Supplier::THUMB_IMAGE_PATH,$supplier['avatar']);
            }
            info('SUPPLIER_STORE_FAILED',['supplier'=>$supplier,'address'=>$address,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return new SupplierEditResource($supplier);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSupplierRequest  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateSupplierRequest $request, Supplier $supplier)
    {
        $supplierEditData = (new Supplier())->prepareData($request->all(),auth());
        $addressEditData = (new Address())->prepareData($request->all());


        if($request->has('avatar')){
            $file = $request->input('avatar');
            $avatarName = Str::slug($request->name).'-'.time();
            $supplierEditData['avatar'] = $this->processImageUpload($request->input('avatar'),$avatarName,$supplier->avatar);
        }

        try {
            DB::beginTransaction();
            $supplier->update($supplierEditData);
            $supplierAddress = $supplier->address()->update($addressEditData);
            DB::commit();
            return response()->json(['msg'=>'Data has been created!','cls'=>'success']);
        }catch (\Throwable $e){
            info('SUPPLIER_STORE_FAILED',['supplier'=>$supplier,'address'=>$supplierAddress,'exception'=>$e]);
            DB::rollBack();
            return response()->json(['msg'=>'Something went wrong!','cls'=>'warning','flag'=>'true']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Supplier $supplier)
    {
        if (!empty($supplier->avatar)){
            ImageUploadManager::deletePhoto(Supplier::IMAGE_PATH,$supplier['avatar']);
            ImageUploadManager::deletePhoto(Supplier::THUMB_IMAGE_PATH,$supplier['avatar']);
        }
        (new Address())->deleteAddressBySupplierId($supplier);
        $supplier->delete();
        return response()->json(['msg'=>'Data has been deleted!','cls'=>'success']);
    }

    private function processImageUpload($file, $name ,$existing_avatar= null)
    {
        $width = 800;
        $height = 800;
        $width_thumb = 150;
        $height_thumb = 150;
        $path  = Supplier::IMAGE_PATH;
        $path_thumb = Supplier::THUMB_IMAGE_PATH;
        if(!empty($existing_avatar))
        {
            ImageUploadManager::deletePhoto(Supplier::IMAGE_PATH,$existing_avatar);
            ImageUploadManager::deletePhoto(Supplier::THUMB_IMAGE_PATH,$existing_avatar);
        }
        $avatar_name = ImageUploadManager::uploadImage($name, $width,$height,$path,$file);
        ImageUploadManager::uploadImage($name, $width_thumb,$height_thumb,$path_thumb,$file);
        return $avatar_name;
    }

    public function getSupplierList()
    {
        $brandList = (new Supplier())->getSuppliersIdAndName();
        return response()->json($brandList);
    }
}
