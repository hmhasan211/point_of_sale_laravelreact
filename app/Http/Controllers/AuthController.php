<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\SalesManager;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public const ADMIN = 1;
    public const SALES_MANAGER = 2;
    final public function login(AuthRequest $request)
    {
        if ($request->input('user_type') == self::ADMIN){
            $user = (new User())->getUserByEmailOrPhone($request->all());
            $role = self::ADMIN;
        }else{
            $user = (new SalesManager())->getUserByEmailOrPhone($request->all());
            $role = self::SALES_MANAGER;
        }

       if ($user && Hash::check($request->input('password'),$user->password)){
           $user_data['token'] = $user->createToken($user->email)->plainTextToken;
           $user_data['name'] = $user->name;
           $user_data['phone'] = $user->phone;
           $user_data['email'] = $user->email;
           $user_data['role'] = $role;
        return response()->json($user_data);
       }
       throw ValidationException::withMessages([
            'email' => ['Provided Credentials are incorrect']
       ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json(['msg'=> 'You have logout successfully!!']);
    }
}
