<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductPhoto;
use App\Http\Requests\StoreProductPhotoRequest;
use App\Http\Requests\UpdateProductPhotoRequest;
use App\Manager\ImageUploadManager;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ProductPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductPhotoRequest  $request
     * @return string
     */
    public function store(StoreProductPhotoRequest $request , $id)
    {
        $product = (new Product())->findProductById($id);
        if ($product) {

            if ($request->input('avatars')) {

                foreach ($request->avatars as $value) {
//                   return $value['photo'];
                    $name = Str::slug($product->slug . '-' . Carbon::now()->toDateTimeString() . '-' . random_int(10000, 99999));
                    $data['product_id'] = $id;
                    $data['is_primary_img'] = $value['is_primary_img'];
                    $data['avatar'] = ImageUploadManager::processImageUpload(
                        $value['photo'],
                        $name,
                        ProductPhoto::MAIN_WIDTH,
                        ProductPhoto::MAIN_HEIGHT,
                        ProductPhoto::MAIN_IMAGE_PATH,
                        ProductPhoto::THUMB_IMAGE_PATH,
                        ProductPhoto::THUMB_WIDTH,
                        ProductPhoto::THUMB_HEIGHT
                    );
                    (new ProductPhoto())->storeProductPhoto($data);
                }
            }
        }
        return response()->json(['msg'=>'Photo Added Successfuly', 'cls'=>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductPhoto  $productPhoto
     * @return \Illuminate\Http\Response
     */
    public function show(ProductPhoto $productPhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductPhoto  $productPhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductPhoto $productPhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductPhotoRequest  $request
     * @param  \App\Models\ProductPhoto  $productPhoto
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductPhotoRequest $request, ProductPhoto $productPhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductPhoto  $productPhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductPhoto $productPhoto)
    {
        //
    }
}
