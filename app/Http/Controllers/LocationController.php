<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    public function getDivisions()
    {
        $divisions =(new Division())->getAllDivisions();
        return response()->json($divisions);
    }

    public function getDistricts($id)
    {
        $districsts = (new District())->getAllDistrictsByDivision($id);
        return response()->json($districsts);
    }

    public function getAreas($id)
    {
        $area = (new Area())->getAreasByDistrict($id);
        return response()->json($area);
    }
}
