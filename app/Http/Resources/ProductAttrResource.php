<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAttrResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'user_id'=>$this->user->name ?? '',
            'created_at'=>Carbon::create($this->created_at->toDateTimeString()),
            'updated_at'=>$this->created_at != $this->updated_at ?  Carbon::create($this->updated_at->toDateTimeString()) : 'Not updated',
            'status'=>$this->status == 1 ? 'active': 'inactive',
            'status_original'=>$this->status,
            'value' => AttrValueResource::collection($this->value)
        ];
    }
}
