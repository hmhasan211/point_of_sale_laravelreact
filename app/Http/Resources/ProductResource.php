<?php

namespace App\Http\Resources;

use App\Models\ProductPhoto;
use App\Manager\ImageUploadManager;
use App\Manager\PriceManager;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'country_id' => $this->country->name ?? null,
            'brand_id' => $this->brand->name ?? null,
            'category_id' => $this->category->name ?? null,
            'sub_category_id' => $this->sub_category->name ?? null,
            'supplier_id' => $this->supplier ? $this->supplier->name.' '.$this->supplier->phone : null,
            'created_by' => $this->created_by->name,
            'updated_by' => $this->updated_by->name,

            'name' => $this->name ?? null,
            'cost' => $this->cost  . PriceManager::CURRENCY_SYMBOL ?? null,
            'sku' => $this->sku ?? null,
            'slug' => $this->slug ?? null,
            'price' => $this->price  . PriceManager::CURRENCY_SYMBOL ?? null,
            'orginal_price' =>  $this->price ?? 0,
            'sell_price' => PriceManager::calculate_sell_price($this->price,$this->discount_percent,$this->discount_fixed,$this->discount_start, $this->discount_end),
            'description' => $this->description ?? null,
            'discount_percent' => $this->discount_percent . '%' ?? null,
            'discount_fixed' => $this->discount_fixed  . PriceManager::CURRENCY_SYMBOL ?? null,
            'discount_start' => $this->discount_start != null ? Carbon::create($this->discount_start)->toDayDateTimeString() : null,
            'discount_end' => $this->discount_end != null ? Carbon::create($this->discount_end)->toDayDateTimeString() : null,
            'status' => $this->status ?? null,
            'stock' => $this->stock ?? null,
            'created_at' =>$this->created_at != null ? Carbon::create($this->created_at)->toDayDateTimeString() : null,
            'updated_at' =>$this->updated_at != null ? Carbon::create($this->updated_at)->toDayDateTimeString() : null,

            'primary_img' => ImageUploadManager::imagePath(ProductPhoto::THUMB_IMAGE_PATH, $this->primary_photo->avatar ?? null ),

            'attributes' => ProductAttributeResource::collection($this->product_attributes),
        ];

    }
}
