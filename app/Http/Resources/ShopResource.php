<?php

namespace App\Http\Resources;

use App\Models\Shop;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'description'=>$this->description,
            'avatar'=>ImageUploadManager::imagePath(Shop::THUMB_IMAGE_PATH, $this->avatar),
            'avatar_full'=>ImageUploadManager::imagePath(Shop::MAIN_IMAGE_PATH, $this->avatar),
            'user_id'=>$this->user->name ?? '',
            'created_at'=>$this->created_at->toDateTimeString(),
            'updated_at'=>$this->created_at != $this->updated_at ?  $this->updated_at ->toDateTimeString() : 'Not updated',
            'status'=>$this->status == 1 ? 'active': 'inactive',
            'address'=> new AddressResource($this->address),
        ];
    }
}
