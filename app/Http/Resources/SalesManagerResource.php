<?php

namespace App\Http\Resources;

use App\Models\SalesManager;
use App\Models\Supplier;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Resources\Json\JsonResource;

class SalesManagerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id'=>$this->id ?? '',
            'name'=>$this->name ?? '',
            'email'=>$this->email ?? '',
            'phone'=>$this->phone ?? '',
            'nid'=>$this->nid ?? '',
            'description'=>$this->description ?? '',
            'avatar'=>ImageUploadManager::imagePath(SalesManager::THUMB_IMAGE_PATH, $this->avatar),
            'avatar_full'=>ImageUploadManager::imagePath(SalesManager::MAIN_IMAGE_PATH, $this->avatar),
            'nid_photo'=>ImageUploadManager::imagePath(SalesManager::THUMB_IMAGE_PATH, $this->nid_photo),
            'nid_photo_full'=>ImageUploadManager::imagePath(SalesManager::MAIN_IMAGE_PATH, $this->nid_photo),
            'user_id'=>$this->user->name ?? '',
            'shop_id'=>$this->shop->name ?? '',
            'created_at'=>$this->created_at->toDateTimeString(),
            'updated_at'=>$this->created_at != $this->updated_at ?  $this->updated_at ->toDateTimeString() : 'Not updated',
            'status'=>$this->status == 1 ? 'active': 'inactive',
            'address'=> new AddressResource($this->address),
        ];
    }
}
