<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttrValueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'status_original'=>$this->status,
            'status'=>$this->status == 1 ? 'Active': 'Inactive',
            'user'=>$this->user->name ?? '',
        ];
    }
}
