<?php

namespace App\Http\Resources;

use App\Models\Supplier;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplierEditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'description'=>$this->description,
            'serial'=>$this->serial,
            'avatar_preview'=> ImageUploadManager::imagePath(Supplier::THUMB_IMAGE_PATH,$this->avatar),
            'status'=> $this->status,
            'address'=> $this->address->address ?? '',
            'division_id'=> $this->address->division_id ?? '',
            'district_id'=> $this->address->district_id ?? '',
            'area_id'=> $this->address->area_id ?? '',
            'landmark'=> $this->address->landmark ?? '',
        ];
    }
}
