<?php

namespace App\Http\Resources;

use App\Manager\ImageUploadManager;
use App\Manager\PriceManager;
use App\Models\ProductPhoto;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => ImageUploadManager::imagePath(ProductPhoto::THUMB_IMAGE_PATH, $this->primary_photo->avatar ?? null ),
            'sku' => $this->sku,
            'cost' =>  $this->cost  . PriceManager::CURRENCY_SYMBOL ?? null,
            'quantity' => $this->quantity,
            'price' => $this->price  . PriceManager::CURRENCY_SYMBOL ?? null,
            'sell_price' => PriceManager::calculate_sell_price($this->price,$this->discount_percent,$this->discount_fixed,$this->discount_start, $this->discount_end),
            'discount_percent' => $this->discount_percent,
            'discount_fixed' => $this->discount_fixed,
            'discount_start' => $this->discount_start,
            'discount_end' => $this->discount_end,
            'category' => $this->category->name ?? '',
            'sub_category' => $this->sub_category->name ?? '',
            'supplier_id' => $this->supplier->name ?? '',
            'brand_id' => $this->brand->name ?? '',
            'created_at' =>$this->created_at != null ? Carbon::create($this->created_at)->toDayDateTimeString() : null,
            'updated_at' =>$this->updated_at != null ? Carbon::create($this->updated_at)->toDayDateTimeString() : null,


        ];
    }
}
