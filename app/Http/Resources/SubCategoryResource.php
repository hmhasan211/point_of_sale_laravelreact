<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\SubCategory;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'category'=>$this->category->name ?? '',
            'name'=>$this->name,
            'slug'=>$this->slug,
            'description'=>$this->description,
            'serial'=>$this->serial,
            'avatar'=>ImageUploadManager::imagePath(SubCategory::THUMB_IMAGE_PATH, $this->avatar),
            'avatar_full'=>ImageUploadManager::imagePath(SubCategory::IMAGE_PATH, $this->avatar),
            'user_id'=>$this->user->name ?? '',
            'created_at'=>$this->created_at->toDateTimeString(),
            'updated_at'=>$this->created_at != $this->updated_at ?  $this->updated_at ->toDateTimeString() : 'Not updated',
            'status'=>$this->status == 1 ? 'active': 'inactive',

        ];
    }
}
