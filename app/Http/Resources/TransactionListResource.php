<?php

namespace App\Http\Resources;

use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'amount'=>$this->amount,
            'trx_id'=>$this->trx_id ,
            'created_at'=>$this->created_at,
            'customer_name'=>$this->customer->name,
            'customer_phone'=>$this->customer->phone,
            'account_number'=>$this->payment_method->account_number,
            'payment_method'=>$this->payment_method->name,
            'status'=> $this->status == Transaction::STATUS_SUCCESS ? 'Success' : 'Failed',
            'transaction_type'=> $this->transaction_type == Transaction::CREDIT ? 'Credit' : 'Debit',
            'transaction_by'=> $this->transactionable->name,
        ];
    }
}
