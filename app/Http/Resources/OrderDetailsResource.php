<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $payment_status = 'Unpaid';
        if ($this->payment_status == Order::PAYMENT_STATUS_PAID){
            $payment_status = 'Paid';
        }elseif ($this->payment_status == Order::PAYMENT_STATUS_PARTIALY_PAID){
            $payment_status = 'Partially Paid';
        }

        return [
            'id' =>$this->id,
            'customer' => new CustomerDetailsResource($this->customer),
            'order_number' =>$this->customer->phone,
            'order_status' =>$this->status,

            'discount' =>$this->discount,
            'due' =>$this->due,
            'quantity' =>$this->quantity,
            'sub_total' =>$this->sub_total,
            'total' =>$this->total,
            'paid' =>$this->paid,

            'order_status_string' =>$this->status == Order::STATUS_COMPLETED ? 'Completed':'Pending',
            'payment_method' => new PaymentMethodDetailsResource($this->payment_method),
            'payment_status' => $payment_status,
            'user' => $this->user->name,
            'shop' => new ShopResource($this->shop),
            'details' => OrderDetailsList::collection( $this->order_details),
            'transactions' => TransactionListResource::collection($this->transactions),

            'created_at' => $this->created_at->toDayDateTimeString(),
            'updated_at' =>$this->created_at != $this->updated_at ? $this->updated_at->toDayDateTimeString() : 'Not Updated'
            ];
    }
}
