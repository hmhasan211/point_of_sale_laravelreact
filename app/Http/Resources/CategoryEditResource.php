<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Manager\ImageUploadManager;
use Illuminate\Http\Resources\Json\JsonResource;
use Intervention\Image\ImageManager;

class CategoryEditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'slug'=>$this->slug,
            'description'=>$this->description,
            'serial'=>$this->serial,
            'avatar_preview'=> ImageUploadManager::imagePath(Category::THUMB_IMAGE_PATH,$this->avatar),
            'status'=> $this->status,
        ];
    }
}
