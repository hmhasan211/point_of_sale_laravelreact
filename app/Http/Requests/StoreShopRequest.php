<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'phone'=>'required|numeric',
            'email'=>'required|email',
            'division_id'=>'required|numeric',
            'district_id'=>'required|numeric',
            'area_id'=>'required|numeric',
            'address'=>'required|max:600',
            'description'=>'max:1000'
        ];
    }
}
