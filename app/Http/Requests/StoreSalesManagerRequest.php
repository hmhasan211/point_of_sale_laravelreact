<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class StoreSalesManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'phone' => 'required|numeric',
            'avatar' => 'required',
            'nid' => 'required',
            'email' => 'email',
            'division_id' => 'numeric',
            'district_id' => 'numeric',
            'area_id' => 'numeric',
            'shop_id' => 'numeric',
            'address' => 'required|max:600',
            'description' => 'max:1000',
            'password' => [
                'required',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
                    ->uncompromised(),
            ],
        ];

    }
}
