<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'phone'=>'required|numeric',
            'email'=>'email',
            'division_id'=>'numeric',
            'district_id'=>'numeric',
            'area_id'=>'numeric',
            'address'=>'max:600',
            'description'=>'max:1000'
        ];
    }
}
