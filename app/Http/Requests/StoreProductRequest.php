<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'country_id' => 'numeric',
            'brand_id' => 'numeric',
            'supplier_id' => 'numeric',
            'category_id' => 'required|numeric',
            'sub_category_id' => 'numeric',
            'created_by_id' => 'numeric',
            'updated_by_id' => 'numeric',
            'name' => 'required|string|max:255|min:3',
            'slug' => 'string|required|max:255|min:3',
            'sku' => 'required|max:255|min:3|unique:products',
            'description' => 'string',
            'cost' => 'numeric|required',
            'stock' => 'numeric',
            'price' => 'numeric|required',
            'discount_percent' => 'numeric',
            'discount_fixed' => 'numeric',
            'status' => 'numeric',
            'discount_start' => 'date',
            'discount_end' => 'date',

            'attributes'=>'array',
            'specifications' =>'array'
        ];
    }
}
