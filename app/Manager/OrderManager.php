<?php

namespace App\Manager;

use App\Models\Product;
use Carbon\Carbon;

class OrderManager
{
    private const ORDER_PREFIX = 'HHC';

    public static function generateOrderNumber($shop_id): string
    {
        return self::ORDER_PREFIX . $shop_id . Carbon::now()->format('dmYHis');
    }

    public static function handleOrderData(array $input): array
    {
        $sub_total = 0;
        $total = 0;
        $discount = 0;
        $quantity = 0;
        $order_details = [];

        if (isset($input['cart'])) {
            foreach ($input['cart'] as $key => $cart) {
                $product = (new Product())->findProductById($key);
                if ($product && $product->stock >= $cart['quantity']) {
                    $price = PriceManager::calculate_sell_price($product->price, $product->discount_percent, $product->discount_fixed, $product->discount_start, $product->discount_end);
                    $sub_total += $product->price * $cart['quantity'];
                    $discount += $price['discount'] * $cart['quantity'];
                    $total += $price['price'] * $cart['quantity'];
                    $quantity += $cart['quantity'];

                    $productData['stock'] = $product->stock - $cart['quantity'];
                    $product->update($productData);
                    $product->quantity = $cart['quantity'];
                    $order_details [] = $product;
                }else{
                    info('PRODUCT_STOCK_OUT',['product'=>$product, 'cart'=>$cart]);
                    return ['error_msg' => $product->name.' stock out or not exist!'];
                    break;
                }
            }
        }
        return [
            'sub_total' => $sub_total,
            'total' => $total,
            'discount' => $discount,
            'quantity' => $quantity,
            'order_details' => $order_details
        ];
    }


    public static function paymentStatus($amount, $paid_amount)
    {
        /*
         * Paid = 1,
         * Partially paid = 2,
         * Unpaid = 3
         */
        $payment_status = 3;
        if ($amount <= $paid_amount){
            $payment_status = 1;
        }elseif ($paid_amount <= 0){
            $payment_status = 3;
        }else{
            $payment_status = 2;
        }
        return $payment_status;
    }
}
