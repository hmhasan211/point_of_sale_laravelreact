<?php

namespace App\Manager;

use Intervention\Image\Facades\Image;

class ImageUploadManager
{
    public const DEFAULT_IMG = 'images/default.png';

    final public static function uploadImage(string $name, int $width, int $height, string $path, string $file): string
    {
        $imageFileName = $name . '.webp';
        Image::make($file)->fit($width, $height)->save(public_path($path) . $imageFileName, 50, 'webp');
        return $imageFileName;
    }

    public static function deletePhoto($path, $img): void
    {
        $path = public_path($path) . $img;
        if ($img != '' && file_exists($path)) {
            unlink($path);
        }
    }

    final public static function imagePath($path, $img)
    {
        $url = url($path . $img);
        if (empty($img)) {
            return $url = url(self::DEFAULT_IMG);
        }
        return $url;
    }

    public static function processImageUpload(
        string $file,
        string $name,
               $width,
               $height,
               $path,
               $path_thumb = null,
               $width_thumb = null,
               $height_thumb = null,
               $existing_avatar = null
    ): string
    {
        if (!empty($existing_avatar)) {
            self::deletePhoto($path, $existing_avatar);
            if (!empty($path_thumb)) {
                self::deletePhoto($path_thumb, $existing_avatar);
            }
        }
        $avatar_name = ImageUploadManager::uploadImage($name, $width, $height, $path, $file);
        if (!empty($path_thumb)) {
            self::uploadImage($name, $width_thumb, $height_thumb, $path_thumb, $file);
        }
        return $avatar_name;
    }
}
