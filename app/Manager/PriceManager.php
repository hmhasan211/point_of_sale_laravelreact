<?php

namespace App\Manager;

use Carbon\Carbon;

class PriceManager
{
    public const CURRENCY_SYMBOL = '৳';
    public const CURRENCY_NAME = 'BDT';

    /**
     * @param $price
     * @param $dis_percent
     * @param $dis_fixed
     * @param $dis_start
     * @param $dis_end
     * @return array|float[]|int[]
     */
    public static function calculate_sell_price($price, $dis_percent, $dis_fixed, $dis_start, $dis_end)
    {
        $discount = 0;
        if (!empty($dis_start && $dis_end)) {
            if (Carbon::now()->isBetween(Carbon::create($dis_start), Carbon::create($dis_end))) {
                return self::calculate_price($price, $dis_percent, $dis_fixed);
            }
        }
        return ['price'=>$price, 'discount'=>$discount,'symbol'=>self::CURRENCY_SYMBOL];
    }

    /**
     * @param $price
     * @param $dis_percent
     * @param $dis_amount
     * @return array|float[]|int[]
     */
    private static function calculate_price($price,$dis_percent, $dis_fixed)
    {
        $discount = 0;
        if(!empty($dis_percent)){
           $discount = ($price * $dis_percent)/100;
        }
        if (!empty($dis_fixed)){
            $discount += $dis_fixed;
        }
        $price = $price - $discount;
        return ['price'=> $price, 'discount'=> $discount,'symbol'=>self::CURRENCY_SYMBOL];
    }
}
