<?php

namespace App\Manager;

use App\Models\Area;
use App\Models\Country;
use App\Models\District;
use App\Models\Division;
use Illuminate\Support\Facades\Http;

class ScriptManager
{
    public function getAllLocation()
    {
       $url = 'https://member.daraz.com.bd/locationtree/api/getSubAddressList?countryCode=BD&page=addressEdit';
       $response = Http::get($url);
       $divisions = json_decode($response->body(),true);

       foreach ($divisions['module'] as $key=>$division){
//           two division insert at a time
           if ( $key == 0 || $key == 1){
               $division_data['name'] = $division['name'];
               $division_data['original_id'] = $division['id'];
               $inserted_div = Division::query()->updateOrCreate($division_data);
               $distUrl =  'https://member.daraz.com.bd/locationtree/api/getSubAddressList?countryCode=BD&addressId='.$division['id'].'&page=addressEdit';
               $response = Http::get($distUrl);
               $districts = json_decode($response->body(),true);

               foreach ($districts['module'] as $district){
                   $district_data['name'] = $district['name'];
                   $district_data['original_id'] = $district['id'];
                   $district_data['division_id'] = $inserted_div->id;
                   $inserted_dist = District::query()->updateOrCreate($district_data);
                   $areaUrl = 'https://member.daraz.com.bd/locationtree/api/getSubAddressList?countryCode=BD&addressId='.$district['id'].'&page=addressEdit';
                   $response = Http::get($areaUrl);
                   $areas = json_decode($response->body(),true);

                   foreach ($areas['module'] as $area){
                       $area_data['name'] = $area['name'];
                       $area_data['original_id'] = $area['id'];
                       $area_data['district_id'] = $inserted_dist->id;
                       Area::query()->updateOrCreate($area_data);
                   }
               }
           }

       }
       echo 'success';

    }

    public function getCountries()
    {
//        $url = 'https://restcountries.com/v3.1/all';
        $url = 'https://countriesnow.space/api/v0.1/countries';
        $response = Http::get($url);
        $response = json_decode($response->body(),true);
        foreach ($response['data'] as $country)
        {
            $country_data['name'] = $country['country'];
            Country::query()->create($country_data);
        }
        return 'success';
    }
}
