<?php

namespace App\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Supplier extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'phone', 'email', 'avatar', 'status', 'description', 'user_id'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public const IMAGE_PATH = 'images/uploads/supplier/';
    public const THUMB_IMAGE_PATH = 'images/uploads/supplier_thumb/';

    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    final function prepareData(array $input, $auth)
    {
        $supplier['name'] = $input['name'] ?? '';
        $supplier['phone'] = $input['phone'] ?? '';
        $supplier['email'] = $input['email'] ?? '';
        $supplier['description'] = $input['description'] ?? '';
        $supplier['user_id'] = $auth->id();
        $supplier['status'] = self::STATUS_ACTIVE;
        return $supplier;
    }


    public function address()
    {
        return $this->morphOne(Address::class,'addressable');
    }

    final public function getAllData (array $input): LengthAwarePaginator
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query()->with(
            'user:id,name',
            'address',
            'address.division:id,name',
            'address.district:id,name',
            'address.area:id,name',
        );
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                    ->orWhere('phone','like','%'.$input['search'].'%')
                    ->orWhere('email','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }


    final public function getSuppliersIdAndName()
    {
        return self::query()
            ->select('id','name','phone','email')
            ->where([['status',self::STATUS_ACTIVE],['user_id', auth()->id()]])
            ->orderBy('name','asc')
            ->get();
    }
}
