<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    use HasFactory;
    protected $fillable = ['product_id','is_primary_img','avatar'];
    public const MAIN_IMAGE_PATH = 'images/uploads/product/';
    public const THUMB_IMAGE_PATH = 'images/uploads/product_thumb/';
    public const MAIN_WIDTH = 800;
    public const MAIN_HEIGHT = 800;
    public const THUMB_WIDTH = 150;
    public const THUMB_HEIGHT= 150;



    /**
     * @param $photo
     * @return mixed
     */
    final public function storeProductPhoto($photo)
    {
        return self::create($photo);
    }
}
