<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $fillable = ['name','email','phone','status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    final public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }
    final public function getAllCustomers($input)
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query();

        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                ->orWhere('phone','like','%'.$input['search'].'%')
                ->orWhere('email','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }
    public function storeCustomer(array $input)
    {
        $customer = $this->prepareData($input);
       return self::query()->create($customer);
    }

    private function prepareData($input)
    {
       return $data = [
            'name' => $input['name'] ?? '',
            'email' => $input['email'] ?? '',
            'phone' => $input['phone'] ?? '',
            'status' => $input['status'] ?? 0,
        ];
    }
}
