<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['country_id', 'brand_id', 'supplier_id', 'sub_category_id', 'created_by_id',
        'updated_by_id', 'name', 'slug', 'sku', 'description', 'cost', 'stock', 'price', 'discount_percent',
        'discount_fixed', 'status', 'discount_start', 'discount_end', 'category_id','attributes','specifications' ];


    /**
     * @param array $input
     * @param $auth_id
     * @return \Illuminate\Database\Eloquent\Builder|Model
     */
    public function storeProduct(array $input, $auth_id)
    {
        return self::query()->create($this->prepareData($input, $auth_id));
    }

    private function prepareData(array $input, $auth_id):array
    {
       return [
          'country_id' => $input['country_id'] ?? null,
          'brand_id' => $input['brand_id'] ?? null,
          'category_id' => $input['category_id'] ?? null,
          'sub_category_id' => $input['sub_category_id'] ?? null,
          'supplier_id' => $input['supplier_id'] ?? null,
          'created_by_id' => auth()->id(),
          'updated_by_id' => auth()->id(),
          'name' => $input['name'] ?? '',
          'cost' => $input['cost'] ?? 0,
          'sku' => $input['sku'] ??  0,
          'slug' => $input['slug'] ? Str::slug($input['slug']) : '',
          'price' => $input['price'] ?? 0,
          'description' => $input['description'] ?? '',
          'discount_percent' => $input['discount_percent'] ?? null,
          'discount_fixed' => $input['discount_fixed'] ?? null,
          'discount_start' => $input['discount_start'] ?? null,
          'discount_end' => $input['discount_end'] ?? null,
          'status' => $input['status'] ?? null,
          'stock' => $input['stock'] ?? null,
        ];
    }

    final public function findProductById($id)
    {
        return self::query()->with('primary_photo')->findOrFail($id);
    }

    final public function getAllProduct($input)
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query()->with([
            'category:id,name',
            'sub_category:id,name',
            'brand:id,name',
            'country:id,name',
            'supplier:id,name,phone',
            'created_by:id,name',
            'updated_by:id,name',
            'primary_photo',
            'product_attributes',
            'product_attributes.attributes',
            'product_attributes.attribute_value',
        ]);
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                   ->orWhere('sku','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }

    final public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    final public function sub_category(): BelongsTo
    {
        return $this->belongsTo(SubCategory::class);
    }
    final public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }
    final public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }
    final public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }
    final public function created_by(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by_id');
    }
    final public function updated_by(): BelongsTo
    {
        return $this->belongsTo(User::class,'updated_by_id');
    }
    final public function primary_photo(): HasOne
    {
        return $this->hasOne(ProductPhoto::class)->where('is_primary_img',1);
    }

    final public function product_attributes(): HasMany
    {
        return $this->hasMany(ProductAttribute::class);
    }


}
