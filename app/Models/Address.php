<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Address extends Model
{
    use HasFactory;
    protected $fillable = [ 'addressable_type','addressable_id','address', 'division_id','landmark', 'district_id', 'area_id', 'status', 'type'];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const SUPPLIER_ADDR = 1;
    const CUSTOMER_PERMENENT_ADDR = 2;
    const CUSTOMER_PRESENT_ADDR = 3;
    final function prepareData(array $input)
    {
        $address['address'] = $input['address'] ?? '';
        $address['division_id'] = $input['division_id'] ?? '';
        $address['district_id'] = $input['district_id'] ?? '';
        $address['area_id'] = $input['area_id'] ?? '';
        $address['landmark'] = $input['landmark'] ?? '';
        $address['status'] = self::STATUS_ACTIVE;
        $address['type'] = self::SUPPLIER_ADDR;
        return $address;
    }

    /**
     * @return MorphTo
     */
    final public function addressable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    final public function division()
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    final public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    final public function area()
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * @param Supplier $supplier
     * @return mixed
     */
    final function deleteAddressBySupplierId(Supplier $supplier)
    {
       return $supplier->address()->delete();
    }
    final function deleteAddressByShopId(SalesManager $salesManager)
    {
       return $salesManager->address()->delete();
    }
}
