<?php

namespace App\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Category extends Model
{
    use HasFactory;

    // images path for category;
    public const IMAGE_PATH = 'images/uploads/category/';
    public const THUMB_IMAGE_PATH = 'images/uploads/category_thumb/';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    protected $fillable = ['name','slug','serial','status','description','avatar','user_id'];

    final public function storeCategory(array $input)
    {
        self::query()->create($input);
    }

    final public function getAllData (array $input): LengthAwarePaginator
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query();
        if (!empty($input['search'])){
           $query->where('name','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
       return $query->with('user:id,name')->paginate($per_page);
    }

    /*
     * only name id pass
     * */
    public static function getCategoryIdAndName()
    {
        return self::query()->select('id','name')
            ->where([['status',self::STATUS_ACTIVE],['user_id',auth()->id()]])
            ->get();
    }

    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
