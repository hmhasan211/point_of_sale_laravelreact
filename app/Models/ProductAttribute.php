<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductAttribute extends Model
{
    use HasFactory;
    protected $fillable = ['product_id','attribute_id','attribute_value_id'];

    public function storeAttributeData(array $input, Product $product)
    {
        $attributes = $this->prepareAttributeData($input, $product);
        foreach ($attributes as $data){
            self::query()->create($data);
        }
    }

    private function prepareAttributeData(array $input, Product $product)
    {
        $attribute_data = [];
        foreach ($input as $value){
            $data['product_id'] = $product->id;
            $data['attribute_id'] = $value['attribute_id'];
            $data['attribute_value_id'] = $value['value_id'];
            $attribute_data[] = $data;
        }
        return $attribute_data;
    }

    final public function attributes(): BelongsTo
    {
        return $this->belongsTo(Attribute::class,'attribute_id');
    }

    final public function attribute_value(): BelongsTo
    {
        return $this->belongsTo(Attribute::class,'attribute_value_id');
    }
}
