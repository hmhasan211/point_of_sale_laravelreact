<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [ 'trx_id', 'order_id', 'customer_id', 'payment_method_id', 'transaction_type', 'amount', 'transactionable_type','status', 'transactionable_id'];
    public const CREDIT = 1;
    public const DEBIT = 2;
    public const STATUS_SUCCESS = 1;
    public const STATUS_FAILED = 0;

    /**
     *
     */
    final public function transactionable()
    {
        return $this->morphTo();
    }
    public function storeTransaction($input, $order, $auth)
    {
        $transaction_data = $this->prepareData($input, $order, $auth);
        return self::query()->create($transaction_data);
    }

    private function prepareData ($input, $order, $auth)
    {
        return [
            'trx_id' => $input['order_summary']['trx_id'] ?? null,
            'order_id' => $order->id ?? null,
            'customer_id' => $input['order_summary']['customer']['cus_id'] ?? null,
            'payment_method_id' => $input['order_summary']['payment_method_id'] ?? null,
            'transaction_type' => self::CREDIT,
            'status' =>self::STATUS_SUCCESS,
            'amount'=> $input['order_summary']['paid'] ?? 0,
            'transactionable_type' => User::class,
            'transactionable_id' => $auth->id
        ];
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
