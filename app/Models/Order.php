<?php

namespace App\Models;

use App\Manager\OrderManager;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    protected $fillable = [
        'customer_id', 'user_id', 'shop_id', 'payment_method_id', 'sub_total', 'total', 'discount', 'quantity', 'paid', 'due', 'status', 'order_number', 'payment_status', 'shipment_status',
    ];

    use HasFactory;

    public const STATUS_PENDING = 1;
    public const STATUS_PROCESS = 2;
    public const STATUS_COMPLETED = 3;
    public const SHIPMENT_STATUS_COMPLETED = 1;

    public const PAYMENT_STATUS_PAID = 1;
    public const PAYMENT_STATUS_PARTIALY_PAID = 2;
    public const PAYMENT_STATUS_UNPAID = 3;


    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    final public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
    final public function payment_method(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class,'payment_method_id');
    }
    final public function order_details(): HasMany
    {
        return $this->hasMany(OrderDetails::class);
    }
    final public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
    final public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
    public function getOrders($input,$auth)
    {
        $per_page = $input['per_page'] ?? 10;

        $query = self::query()->with([
            'customer:id,name,phone',
            'payment_method:id,name',
            'user:id,name',
            'shop:id,name',
        ])->where('user_id',$auth);
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                ->orWhere('phone','like','%'.$input['search'].'%')
                ->orWhere('email','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }
    public function placeOrder(array $input, $auth)
    {
        $order_data = $this->prepareData($input, $auth);
         if (isset($order_data['error_msg'])){
             return $order_data;
         }
       $order = self::query()->create($order_data['order_data']);
        (new OrderDetails())->storeOrderDetails(($order_data['order_details']), $order);
        (new Transaction())->storeTransaction($input, $order, $auth);
        return $order;
    }

    private function prepareData($input, $auth)
    {
        $price = OrderManager::handleOrderData($input);
        if (isset($price['error_msg'])) {
            return $price;
        }else{
            $order_data = [
                'customer_id' => $input['order_summary']['customer']['cus_id'],
//                'sales_manager_id' => $auth->id ?? $auth->salesManager->id,
                'user_id' => $auth->id ,
                'shop_id' => $auth->shop_id ?? $auth->salesManager->shop_id,
                'sub_total' => $price['sub_total'],
                'total' => $price['total'],
                'discount' => $price['discount'],
                'quantity' => $price['quantity'],
                'paid' => $input['order_summary']['paid'] ?? 0,
                'due' => $input['order_summary']['due'] ?? 0,
                'status' => self::STATUS_COMPLETED,
                'order_number' => OrderManager::generateOrderNumber($auth->shop_id),
                'payment_method_id' => $input['order_summary']['payment_method_id'],
                'payment_status' => OrderManager::paymentStatus($price['total'], $input['order_summary']['paid']),
                'shipment_status' => self::SHIPMENT_STATUS_COMPLETED,
            ];
            return ['order_data' => $order_data , 'order_details'=> $price['order_details']];
        }

    }
}
