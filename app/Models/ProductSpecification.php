<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSpecification extends Model
{
    use HasFactory;

    protected $fillable = ['product_id','name','value'];

    final public function storeProductSpecification(array $input, Product $product)
    {
        $specifications = $this->prepareSpecificationData($input, $product);
        foreach ($specifications as $data){
            self::query()->create($data);
        }

    }

    final public function prepareSpecificationData(array $input, Product $product) :array
    {
        $specification_data = [];
        foreach ($input as $value)
        {
            $data['product_id'] = $product->id;
            $data['name'] = $value['name'];
            $data['value'] = $value['value'];
            $specification_data[] = $data;
        }
        return $specification_data;
    }
}
