<?php

namespace App\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Brand extends Model
{
    use HasFactory;
    protected $fillable = ['name','slug','serial','description','status','avatar','user_id'];
    // images path for category;
    public const IMAGE_PATH = 'images/uploads/brand/';

    public const THUMB_IMAGE_PATH = 'images/uploads/brand_thumb/';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    final public function getAllData (array $input): LengthAwarePaginator
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query();
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->with('user:id,name')->paginate($per_page);
    }

    final public function storeBrand(array $input)
    {
        self::query()->create($input);
    }


    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    final public function getBrandIdAndName()
    {
        return self::query()
            ->select('id','name')
            ->where('status',self::STATUS_ACTIVE)
            ->orderBy('id','desc')
            ->get();
    }

}
