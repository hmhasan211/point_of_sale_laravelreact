<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class OrderDetails extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
    public function storeOrderDetails( array $order_details , $order)
    {
        foreach ($order_details as $product){
             $order_details_data = $this->prepareData($product , $order);
             self::query()->create($order_details_data);
        }
    }

    private function prepareData( $product , $order)
    {
        return [
            'order_id' => $order->id ?? null,
            'brand_id' => $product['brand_id'] ?? null,
            'category_id' => $product['category_id'] ?? null,
            'sub_category_id' => $product['sub_category_id'] ?? null,
            'supplier_id' => $product['supplier_id'] ?? null,
            'name' => $product['name'] ?? '',
            'cost' => $product['cost'] ?? 0,
            'sku' => $product['sku'] ??  0,
            'price' => $product['price'] ?? 0,
            'discount_percent' => $product['discount_percent'] ?? null,
            'discount_fixed' => $product['discount_fixed'] ?? null,
            'discount_start' => $product['discount_start'] ?? null,
            'discount_end' => $product['discount_end'] ?? null,
            'quantity' => $product['quantity'] ?? null,
            'avatar' => $product['primary_photo']['avatar'] ?? null,
        ];
    }
}
