<?php

namespace App\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class SalesManager extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name', 'phone', 'email', 'avatar','nid_photo','nid','shop_id','password', 'status', 'description', 'user_id'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public const MAIN_IMAGE_PATH = 'images/uploads/sales_manager/';
    public const THUMB_IMAGE_PATH = 'images/uploads/sales_manager_thumb/';
    public const MAIN_WIDTH = 800;
    public const MAIN_HEIGHT = 800;
    public const THUMB_WIDTH = 150;
    public const THUMB_HEIGHT= 150;

    /**
     * @return BelongsTo
     */
    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    final public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @return MorphOne
     */
    public function address()
    {
        return $this->morphOne(Address::class,'addressable');
    }

    final function prepareData(array $input, $auth)
    {
        $data['name'] = $input['name'] ?? null;
        $data['phone'] = $input['phone'] ?? null;
        $data['email'] = $input['email'] ?? null;
        $data['description'] = $input['description'] ?? null;
        $data['user_id'] = $auth->id();
        $data['shop_id'] = $input['shop_id'] ?? null;
        $data['nid'] = $input['nid'] ?? null;
        $data['password'] = Hash::make($input['password'] ?? 'admin123') ;
        $data['status'] = $input['status'] ?? 0;
        return $data;
    }

    /**
     * @param array $input
     * @return LengthAwarePaginator
     */
    final public function getAllData (array $input): LengthAwarePaginator
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query()->with([
            'user:id,name',
            'shop:id,name',
            'address',
            'address.division:id,name',
            'address.district:id,name',
            'address.area:id,name',
            ]);
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                ->orWhere('phone','like','%'.$input['search'].'%')
                ->orWhere('email','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }

    /**
     * @param array $input
     * @return Builder|Model|object|null
     */
    final public function getUserByEmailOrPhone(array $input)
    {
        return self::query()->where('email',$input['email'])->first();
    }
}
