<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;
    protected $table = 'attributes';
    protected $fillable  = ['name','status','user_id'];

    public function getAllAtrr()
    {
        return self::query()->with(['user','value'])->paginate(10);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function value()
    {
        return $this->hasMany(AttributeValue::class,'attribute_id')->orderBy('id','desc');
    }

    final public function getAttributeIdAndName()
    {
        return self::query()
            ->with('value:id,name,attribute_id')
            ->select('id','name')
            ->orderBy('id','desc')
            ->get();
    }
}
