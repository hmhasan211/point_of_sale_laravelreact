<?php

namespace App\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Shop extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'phone', 'email', 'avatar', 'status', 'description', 'user_id'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public const MAIN_IMAGE_PATH = 'images/uploads/shop/';
    public const THUMB_IMAGE_PATH = 'images/uploads/shop_thumb/';
    public const MAIN_WIDTH = 800;
    public const MAIN_HEIGHT = 800;
    public const THUMB_WIDTH = 150;
    public const THUMB_HEIGHT= 150;


    final public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    final function prepareData(array $input, $auth)
    {
        $shop['name'] = $input['name'] ?? '';
        $shop['phone'] = $input['phone'] ?? '';
        $shop['email'] = $input['email'] ?? '';
        $shop['description'] = $input['description'] ?? '';
        $shop['user_id'] = $auth->id();
        $shop['status'] = self::STATUS_ACTIVE;
        return $shop;
    }


    public function address()
    {
        return $this->morphOne(Address::class,'addressable');
    }

    final public function getAllData (array $input): LengthAwarePaginator
    {
        $per_page = $input['per_page'] ?? 10;
        $query = self::query()->with(
            'user:id,name',
            'address',
            'address.division:id,name',
            'address.district:id,name',
            'address.area:id,name',
        );
        if (!empty($input['search'])){
            $query->where('name','like','%'.$input['search'].'%')
                ->orWhere('phone','like','%'.$input['search'].'%')
                ->orWhere('email','like','%'.$input['search'].'%');
        }
        if (!empty($input['order_by'])){
            $query->orderBy($input['order_by'],$input['direction'] ?? 'asc');
        }
        return $query->paginate($per_page);
    }


    final public function getShopIdAndName()
    {
        return self::query()
            ->select('id','name','phone')
            ->where('status',self::STATUS_ACTIVE)
            ->orderBy('name','asc')
            ->get();
    }
}
