<?php

use App\Http\Controllers\AttributeController;
use App\Http\Controllers\AttrValueController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductPhotoController;
use App\Http\Controllers\SalesManagerController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\SupplierController;
use App\Manager\ScriptManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/test', [ScriptManager::class,'getAllLocation']);
//Route::get('/country', [ScriptManager::class,'getCountries']);

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('divisions', [LocationController::class, 'getDivisions']);
Route::get('districts/{id}', [LocationController::class, 'getDistricts']);
Route::get('areas/{id}', [LocationController::class, 'getAreas']);


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('category/list', [CategoryController::class, "getCategoryList"]); /* get category for Sub category*/
    Route::get('sub-category/list/{cat_id}', [SubCategoryController::class, "getSubCategoryList"]); /* get sub category for Product */
    Route::get('brand/list', [BrandController::class, "getBrandList"]); /* get brand for Product */
    Route::get('supplier/list', [SupplierController::class, "getSupplierList"]); /* get supplier for Product */
    Route::get('attribute/list', [AttributeController::class, "getAttributeList"]); /* get supplier for Product */
    Route::get('shop/list', [ShopController::class, "getShopList"]);      /* get shops for Sales-manager */
    Route::get('countries/list', CountryController::class);      /* get countries for Product */
    Route::post('product-photo/{id}', [ProductPhotoController::class, "store"]);

    Route::apiResource('category', CategoryController::class);
    Route::apiResource('sub-category', SubCategoryController::class);
    Route::apiResource('brand', BrandController::class);
    Route::apiResource('supplier', SupplierController::class);

    Route::apiResource('attr', AttributeController::class);
    Route::apiResource('attr-value', AttrValueController::class);
    Route::apiResource('product', ProductController::class);
    Route::apiResource('shop', ShopController::class);
    Route::apiResource('sales-manager', SalesManagerController::class);
    Route::apiResource('customer', CustomerController::class);
    Route::apiResource('order', OrderController::class);
    Route::apiResource('payment-method', PaymentMethodController::class);

});

//Route::group(['middleware' => ['auth:admin,sales_manager']],function (){
//    Route::apiResource('product', ProductController::class)->only('index','show');
//});

//Route::group(['middleware' => ['auth:sales_manager']],function (){
//    Route::apiResource('product', ProductController::class)->only('index','show');
//});
